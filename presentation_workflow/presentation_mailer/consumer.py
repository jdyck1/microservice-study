import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:

        def process_approval(ch, method, properties, body):
            print(" [X] Received %r" % body)
            content = json.loads(body)
            name = content["presenter_name"]
            email = content["presenter_email"]
            from_email = "admin@conference.go"
            title = content["title"]
            subject = "Your presentation has been accepted"
            message = (
                name
                + ", we're happy to tell you that your presentation "
                + title
                + " has been accepted"
            )
            send_mail(
                subject=subject,
                message=message,
                from_email=from_email,
                recipient_list=[email],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            print(" [X] Received %r" % body)
            content = json.loads(body)
            name = content["presenter_name"]
            email = content["presenter_email"]
            from_email = "admin@conference.go"
            title = content["title"]
            subject = "Your presentation has been rejected"
            message = (
                name
                + ", we regret to inform you that your presentation "
                + title
                + " sucks"
            )
            send_mail(
                subject=subject,
                message=message,
                from_email=from_email,
                recipient_list=[email],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("AMQP connection error")
        time.sleep(2.0)
